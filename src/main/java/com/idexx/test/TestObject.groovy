package com.idexx.test

import org.apache.log4j.Logger;

abstract class TestObject {

    static Logger log4j;

    void error(Object error) {
        if (!getLog4j().is(null)) {
            getLog4j().error(error)
        }
    }

    String toString(Object[] objectsToAppend) {
        StringBuilder sb = new StringBuilder(getClass().getSimpleName());
        sb.append(' -> [');
        for (int index = 0; index < objectsToAppend.size(); index++) {
            Object objectToAppend = objectsToAppend[index]
            sb.append(objectToAppend);
            if (index < objectsToAppend.size() - 1) {
                sb.append(', ');
            }
        }
        sb.append(']');
    }

    abstract protected Object[] getObjectsToDisplay();

    String toString() {
        return toString(getObjectsToDisplay())
    }
}