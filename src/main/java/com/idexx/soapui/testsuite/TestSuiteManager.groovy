package com.idexx.soapui.testsuite

import com.eviware.soapui.model.project.Project
import com.eviware.soapui.model.testsuite.TestStep
import com.eviware.soapui.model.testsuite.TestSuite
import com.eviware.soapui.model.testsuite.TestSuiteRunContext
import com.idexx.soapui.model.TestModelItemManager
import com.idexx.soapui.properties.PropertyManager
import org.apache.log4j.Logger

public class TestSuiteManager extends TestModelItemManager {

	TestSuite testSuite;

	TestSuiteManager (TestSuite testSuite, TestSuiteRunContext context, Logger logger) {
		super (context, logger);
    	setTestSuite (testSuite);
	}

	TestStep getCurrentStep () {
		return null
	}

	@Override
	Project getProject(){
		return getTestSuite().getProject()
	}

	@Override
	public String getBasicPropertyViaModelItem(String propertyName) {
		getLogger().info('Requested basic property' + ' -> ' + propertyName);
		String property = PropertyManager.getProperty(propertyName , (TestSuite) getTestSuite());
		getLogger().info(propertyName  + ' -> ' + property);

		return property
	}

	@Override
	protected Object [] getObjectsToDisplay() {
		return [getTestSuite ()] as Object [];
	}
}
