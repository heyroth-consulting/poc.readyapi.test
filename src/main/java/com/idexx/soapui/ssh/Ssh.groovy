package com.idexx.soapui.ssh

import com.idexx.test.TestObject
import com.jcraft.jsch.*

public class Ssh extends TestObject {
	
	StringBuilder stdout = new StringBuilder()
	String knownHostsFilename
	ArrayList commands
	String password
	String user
	String host
	
	@Override
	protected Object [] getObjectsToDisplay() {
		Object [] objectsToDisplay = [
			"${getUser()}@${getHost()}/${getPassword()}", 
			getCommands()
		]
		
		return objectsToDisplay;
	}
	
	static String userHome = "${System.getProperty('user.home')}";
	static String defaultKnownHostsFilename = "${userHome}/.ssh/known_hosts";
	static String defaultPrivatSshKeyFilename = "${userHome}/.ssh/id_rsa";
	
	public static int executeShellCommand(String command, String user, String password, String host) {
		return executeShellCommands ([command] as String [], user, password, host);
	}

	public static int executeShellCommands(String [] commands, String user, String password, String host) {
		return executeShellCommands(commands, user, password, host, getDefaultKnownHostsFilename ());
	}

	public static int executeShellCommands(String [] commands, String user, String password, String host, String knownHostsFilename) {
		Ssh ssh = new Ssh(commands, user, password, host, knownHostsFilename);
		return ssh.executeCommands();
	}

	public void enableLogging() {
		getLogger().setEnabled(true);
	}

	public void disableLogging() {
		getLogger().setEnabled(false);
	}

	private Ssh() {
	};

	public Ssh(SshConnectionInfo sci) {
		this(sci, getDefaultKnownHostsFilename());
	}

	public Ssh(SshConnectionInfo sci, String knownHostsFilename) {
		this(sci.getCommands(), sci.getUser(), sci.getPassword(), sci.getHost(), knownHostsFilename);
		getLogger().setLog4jLogger(sci.getLogger());
	}

	public Ssh(ArrayList commands, String user, String password, String host, String knownHostsFilename) {
		setCommands(commands);
		setUser(user);
		setPassword(password);
		setHost(host);
		setKnownHostsFilename(knownHostsFilename);
	};

	public Ssh(ArrayList commands, String user, String password, String host) {
		this(commands, user, password, host, getDefaultKnownHostsFilename ());
	}

	public Ssh(String command, String user, String password, String host, String knownHostsFilename) {
		this([command], user, password, host, knownHostsFilename);
	}

	public Ssh(String command, String user, String password, String host) {
		this(command, user, password, host, getDefaultKnownHostsFilename ());
	}

	private Session getSession(JSch jsch) throws JSchException {
        String privateKey = getDefaultPrivatSshKeyFilename ();
		jsch.addIdentity(privateKey);
		Session session = jsch.getSession(getUser(), getHost(), 22);
		session.setConfig("StrictHostKeyChecking", "no");
		session.setPassword(getPassword());
		return session;
	}

	private int executeCommand(String command, Session session) throws IOException, JSchException {
		int exitStatus = -1;
		Channel channel = session.openChannel("exec");
		((ChannelExec) channel).setCommand(command);
		channel.setInputStream(null);
		((ChannelExec) channel).setErrStream(System.err);
		InputStream is = channel.getInputStream();
		channel.connect();
		getStdout().setLength(0);
		byte[] tmp = new byte[1024];
		while (true) {
			while (is.available() > 0) {
				int i = is.read(tmp, 0, 1024);
				if (i < 0)
					break;
				String out = new String(tmp, 0, i);
				getStdout().append(out);
				getLogger().log(out, SshLogger.INFO);
			}
			if (channel.isClosed()) {
				exitStatus = channel.getExitStatus();
				getLogger().log("Exit status : " + exitStatus, SshLogger.ERROR);
				break;
			}
			try {
				Thread.sleep(1000);
			} catch (Exception ee) {
			}
		}
		channel.disconnect();
		return exitStatus;
	}

	public int executeCommands() {
		int exitStatus = -1;
		try {
			JSch.setLogger(getLogger());
			getJsch().setKnownHosts(getKnownHostsFilename());
			Session session = getSession(getJsch());
			session.connect();
			for (String command : getCommands()) {
				getLogger().log('Current command -> ' + command, SshLogger.INFO);
				if ((exitStatus = executeCommand(command, session)) != 0)
					return exitStatus;
			}
			session.disconnect();
		} catch (Exception e) {
			getLogger().log(e.getMessage(), SshLogger.ERROR);
		}
		return exitStatus;
	}

	public int upload(String localAbsoluteFilename, String remoteAbsoluteFilename) {
		int exitStatus = -1;
		try {
			JSch.setLogger(getLogger());
			getJsch().setKnownHosts(getKnownHostsFilename());
			Session session = getSession(getJsch());
			session.connect();
			getLogger().log('Uploading ' + localAbsoluteFilename + ' -> ' + remoteAbsoluteFilename, SshLogger.INFO);
			ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
			channel.connect();
			channel.put(localAbsoluteFilename, remoteAbsoluteFilename);
			channel.disconnect();
			session.disconnect();
			exitStatus = 0;
		} catch (Throwable e) {
			getLogger().log("Upload error -> " +
					"local: " + localAbsoluteFilename + ", " +
					"remote: " + remoteAbsoluteFilename + ", " +
					"errormessage: " + e.getMessage(),
					SshLogger.ERROR);
			throw e
		}
		return 0;
	}

	public int uploadString (String stringToBeUploaded, String remoteAbsoluteFilename, File tempFile, Boolean keepTempFile) {
		int exitStatus = -1;

		try {
			FileWriter fw = new FileWriter(tempFile.getAbsolutePath())
			fw.write(stringToBeUploaded.toCharArray())
			fw.close()
			
			exitStatus = upload (tempFile.getAbsolutePath(), remoteAbsoluteFilename)
		}
		catch (Throwable t){
			throw t
		}
		finally {
			if (! keepTempFile) {
				tempFile.delete()
			}
		}
		return exitStatus;
	}

	public int uploadString (String stringToBeUploaded, String remoteAbsoluteFilename, String tempFileName) {
		String tempPathName = System.getProperty("java.io.tmpdir")
		File tempFile = new File ("${tempPathName}${tempFileName}")
		getLogger().log("UDK-File will be ratained as -> $tempFile", 0)
		return uploadString(stringToBeUploaded, remoteAbsoluteFilename, tempFile, true);
	}

	public int uploadString (String stringToBeUploaded, String remoteAbsoluteFilename) {
		try {
			File tempFile = File.createTempFile("~soapui~", ".tmp")
			return uploadString(stringToBeUploaded, remoteAbsoluteFilename, tempFile, false)
		}
		catch (Throwable t){
			throw t
		}
	}

	public int download(String localAbsoluteFilename, String remoteAbsoluteFilename) {
		int exitStatus = -1;
		try {
			JSch.setLogger(getLogger());
			getJsch().setKnownHosts(getKnownHostsFilename());
			Session session = getSession(getJsch());
			session.connect();
			getLogger().log('download ' + localAbsoluteFilename + ' <- ' + remoteAbsoluteFilename, SshLogger.INFO);
			ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
			channel.connect();
			channel.get(remoteAbsoluteFilename, localAbsoluteFilename);
			channel.disconnect();
			session.disconnect();
			exitStatus = 0;
		} catch (Exception e) {
			getLogger().log("Upload error -> " +
					"local: " + localAbsoluteFilename + ", " +
					"remote: " + remoteAbsoluteFilename + ", " +
					"errormessage: " + e.getMessage(),
					SshLogger.ERROR);
		}
		return exitStatus;
	}

	public byte[] downloadBytes (String remoteAbsoluteFilename) {
		int exitStatus = -1;
		File tmpFile;

		try {
			tmpFile = File.createTempFile("~soapui~", ".tmp")

			download (tmpFile.getAbsolutePath(), remoteAbsoluteFilename)

			FileInputStream fin = new FileInputStream(tmpFile);
			byte [] bb = new byte [tmpFile.length()]
			fin.read(bb)
			fin.close()
			
			return bb
		}
		catch (Throwable t){
			throw t
		}
		finally {
			tmpFile.delete()
		}

		return null;
	}

	public String downloadString (String remoteAbsoluteFilename) {
		return new String (downloadBytes(remoteAbsoluteFilename))
	}
	
	private JSch jsch
	private JSch getJsch() {
		if (jsch == null) {
			jsch = new JSch()
		}
		return jsch
	}

	private SshLogger logger
	private SshLogger getLogger() {
		if (logger == null) {
			logger = new SshLogger(false)
		}
		return logger
	}

	public void setCommand(String command) {
		setCommands([command])
	}
	
	public getStdoutString(){
		return getStdout().toString()
	}
}