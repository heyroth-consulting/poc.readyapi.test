package com.idexx.soapui.ssh;

import com.jcraft.jsch.UserInfo;

public abstract class SshUserInfo implements UserInfo {

	String passwd

    public String getPassphrase() {
        return null;
    }

    public boolean promptPassword(String message) {
        return false;
    }

    public boolean promptPassphrase(String message) {
        return false;
    }

    public boolean promptYesNo(String message) {
        return false;
    }

    public void showMessage(String message) {
    }
}