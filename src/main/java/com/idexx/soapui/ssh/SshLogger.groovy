package com.idexx.soapui.ssh

import com.jcraft.jsch.Logger

public class SshLogger implements Logger {

	private boolean enabled
	org.apache.log4j.Logger log4jLogger

	org.apache.log4j.Logger getLog4jLogger (){
		return log4jLogger
	}

	boolean isEnabled() {
		return enabled
	}

	boolean isEnabled(int level) {
		return isEnabled()
	}

	void log(String message, int level) {
		if (isEnabled(level)) {
			log(level, message)
		}
	}

	void log(int level, String message) {
		org.apache.log4j.Logger l4jl = getLog4jLogger();
		if (l4jl == null) {
			System.out.println(message);
		} else {
			l4jl.info(message)
		}
	}

	void setEnabled(boolean enabled) {
		this.enabled = enabled
	}

	private void setLog4jLogger(org.apache.log4j.Logger log4jLogger){
		this.log4jLogger = log4jLogger
	}

	private SshLogger() {
	}

	SshLogger(boolean enabled) {
		setEnabled(enabled)
	}

	SshLogger(boolean enabled, org.apache.log4j.Logger log4jLogger) {
		this.(enabled)
		setLog4jLogger(log4jLogger)
	}
}
