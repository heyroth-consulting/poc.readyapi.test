package com.idexx.soapui.ssh

import com.eviware.soapui.model.testsuite.TestStep
import com.idexx.soapui.model.TestModelItemManagerInterface
import com.idexx.soapui.properties.PropertyCategory
import com.idexx.soapui.properties.PropertyManager
import com.idexx.test.TestObject
import org.apache.log4j.Logger

class SshConnectionInfo extends TestObject{

	TestModelItemManagerInterface manager

	String hostHint
	
	ArrayList commands

	String host
	String password
	String user
	Logger logger
	
	@Override
	protected Object [] getObjectsToDisplay() {
		Object [] objectsToDisplay = [
			"credentials -> ${getUser()}@${getHost()}/${getPassword()}",
			"commands -> ${getCommands()}"
		] as Object [];
	
		return objectsToDisplay;
	}

	public String getHost (){
		if (host == null){
			String key = getHostHint() + ".host"
			host = getPropertyManager().getProperty(
					PropertyCategory.ENVIRONMENT,
					key,
					(TestStep) getManager().getCurrentStep())
		}
		return host;
	}

	Logger getLogger () {
		if (logger == null) {
			logger = getManager() ?. getLogger() 			
		}
		
		return logger
	}

	public String getPassword() {
		if (password == null){
			String key = getHostHint() + ".password." + getUser()
			password = getPropertyManager().getProperty(
					PropertyCategory.ENVIRONMENT,
					key,
					(TestStep) getManager().getCurrentStep())
		}
		return password
	}

	private PropertyManager getPropertyManager (){
		return getManager().getPropertyManager()
	}

	SshConnectionInfo (String host,	String user, String password, Logger logger){
		 this.host = host
		 this.user = user
		 this.password = password
		 this.logger = logger
	}

	SshConnectionInfo (String hostHint, String user, String command, TestModelItemManagerInterface manager){
		this(hostHint, user, [command], manager);
	}

	SshConnectionInfo (String hostHint, String user, ArrayList commands,TestModelItemManagerInterface manager){
		setManager(manager);

		setHostHint(hostHint);
		setUser(user);
		setCommands(commands);
	}
}
