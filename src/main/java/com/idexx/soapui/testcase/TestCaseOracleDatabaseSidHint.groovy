package com.idexx.soapui.testcase

enum TestCaseOracleDatabaseSidHint {

    SID_HINT_DEFAULT("default");

    String hint

    public TestCaseOracleDatabaseSidHint(String hint) {
        setHint(hint);
    }

    public String toString() {
        StringBuffer sb = new StringBuffer(getClass().getSimpleName() + " -> [");
        sb.append(getHint());
        sb.append("]");

        return sb.toString();
    }
}