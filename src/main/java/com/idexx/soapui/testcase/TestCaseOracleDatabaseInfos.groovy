package com.idexx.soapui.testcase

import com.eviware.soapui.model.testsuite.TestStep
import com.idexx.soapui.properties.PropertyCategory
import com.idexx.soapui.properties.PropertyManager
import com.idexx.test.TestObject

class TestCaseOracleDatabaseInfos extends TestObject {
    private static Vector<String> validSidHints;

    static Vector<String> getValidSidHints() {
        if (validSidHints.is(null)) {
            validSidHints = new Vector<String>();
            for (TestCaseOracleDatabaseSidHint hint : TestCaseOracleDatabaseSidHint.values()) {
                validSidHints.add(hint.getHint())
            }
        }
        return validSidHints
    }

    TestCaseOracleDatabaseInfos(TestCaseManager testCaseManager) {
        PropertyManager pm = testCaseManager.getPropertyManager();
        PropertyCategory pc = PropertyCategory.ENVIRONMENT;
        TestStep ts = testCaseManager.getCurrentStep();

        setPropertyManager(pm);
        setTestCaseManager(testCaseManager);

        String useSidString = pm.getProperty(pc, "db.use.sid", (TestStep) ts)
        setUseSid(new Boolean(useSidString))

        for (String serviceOrSidHint : getValidSidHints()) {
            getDbHosts().put(serviceOrSidHint, pm.getProperty(pc, "db.host." + serviceOrSidHint, (TestStep) ts))
            getDbPorts().put(serviceOrSidHint, pm.getProperty(pc, "db.port." + serviceOrSidHint, (TestStep) ts))
            getServicesOrSIds().put(serviceOrSidHint, pm.getProperty(pc, "db.service.or.sid." + serviceOrSidHint, (TestStep) ts))
        }
    }

    @Override
    protected Object[] getObjectsToDisplay() {
        Vector<String> connectionStrings = new Vector<String>();

        getValidSidHints().each {
            serviceOrSidHint ->
                StringBuffer sb = new StringBuffer();
                sb.append(getHost(serviceOrSidHint))
                sb.append(":");
                sb.append(getPort(serviceOrSidHint));
                sb.append(", ");
                sb.append(getServiceOrSid(serviceOrSidHint));

                connectionStrings.add(sb.toString())
        }

        return connectionStrings as Object[]
    }

    public String getConnectionString(String user, String serviceOrSidHint) {
        StringBuffer sb = new StringBuffer("jdbc:oracle:thin:");
        if (user) {
            sb.append(user);
            sb.append("/PASS_VALUE");
        }
        sb.append("@");
        sb.append(getHost(serviceOrSidHint));
        sb.append(":");
        sb.append(getPort(serviceOrSidHint));
        sb.append(getUseSid() ? ":" : "/");
        sb.append(getServiceOrSid(serviceOrSidHint));

        return sb.toString();
    }

    public String getConnectionString(String serviceOrSidHint) {
        return getConnectionString(null, serviceOrSidHint);
    }

    public String getPassword(String user) {
        PropertyManager pm = getPropertyManager();
        PropertyCategory pc = PropertyCategory.ENVIRONMENT;
        TestStep ts = getTestCaseManager().getCurrentStep();

        return pm.getProperty(pc, "db.password." + user, (TestStep) ts);
    }

    TestCaseManager tcm;

    private TestCaseManager getTestCaseManager() {
        return tcm;
    }

    private String setTestCaseManager(TestCaseManager tcm) {
        this.tcm = tcm;
    }

    PropertyManager propertyManager;

    private PropertyManager getPropertyManager() {
        return propertyManager;
    }

    private String setPropertyManager(PropertyManager propertyManager) {
        this.propertyManager = propertyManager;
    }

    private Boolean useSid;

    Boolean getUseSid() {
        return useSid
    }

    void setUseSid(Boolean useSid) {
        this.useSid = useSid
    }

    private Hashtable<String, String> dbHosts = new Hashtable<String, String>();

    public Hashtable<String, String> getDbHosts() {
        return dbHosts
    }

    private String getHost(String sidHint) {
        return getDbHosts().get(sidHint);
    }

    private Hashtable<String, String> dbPorts = new Hashtable<String, String>();

    public Hashtable<String, String> getDbPorts() {
        return dbPorts
    }

    public String getPort(String sidHint) {
        return getDbPorts().get(sidHint);
    }

    private Hashtable<String, String> servicesOrSids = new Hashtable<String, String>();

    public Hashtable<String, String> getServicesOrSIds() {
        return servicesOrSids
    }

    public String getServiceOrSid(String serviceOrSidHint) {
        return getServicesOrSIds().get(serviceOrSidHint);
    }
}
