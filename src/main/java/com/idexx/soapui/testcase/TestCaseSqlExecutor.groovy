package com.idexx.soapui.testcase

import com.eviware.soapui.model.testsuite.TestCase
import com.eviware.soapui.model.testsuite.TestCaseRunContext
import groovy.sql.Sql
import org.apache.log4j.Logger

import java.sql.SQLException

class TestCaseSqlExecutor {

	Sql sql
	ArrayList commands
	TestCaseManager testCaseManager
	TestCaseDatabaseConnectionManager testCaseJdbcConnectionManager
	
	TestCaseSqlExecutor(TestCaseManager tcm){
		setTestCaseManager(tcm)
		setTestCaseJdbcConnectionManager(new TestCaseDatabaseConnectionManager (tcm))
	}

	TestCaseSqlExecutor(TestCaseRunContext context, Logger logger, TestCase  testCase){
		setTestCaseManager(new TestCaseManager (context, logger))
		getTestCaseManager().setTestCase(testCase)
		setTestCaseJdbcConnectionManager(new TestCaseDatabaseConnectionManager (testCaseManager))
	}

	TestCaseSqlExecutor(TestCaseRunContext context, Logger logger) {
		this (context, logger, context.getTestCase())
	}

	void destroySql () {
		Sql tmpSql = getSql ()
		if (tmpSql != null){
			tmpSql.close()
			setSql (null)
		}
	}

	void createSql (String user, String sidHint) {
		def dbInfos = getTestCaseJdbcConnectionManager().getDbInfos()
		def props = [user: user, password: dbInfos.getPassword(user)] as Properties
		getLogger().info 'Connection Properties -> ' + props
		def url = testCaseJdbcConnectionManager.getDbInfos().getConnectionString(user, sidHint)
		getLogger().info 'Connection String -> ' + url
		def driver = TestCaseDatabaseConnectionManager.jdbcDriverName;
		getLogger().info "Driver -> $driver"
		Sql tmpSql
		try {
			com.eviware.soapui.support.GroovyUtils.registerJdbcDriver(driver);
			getLogger().info "JDBC driver successfully registered"
			tmpSql = Sql.newInstance(url, props, driver)
			getLogger().info "Sql successfully created -> $tmpSql"
			setSql (tmpSql )
		} catch (SQLException sqlE) {
			getLogger().error sqlE.toString()
			throw sqlE
		} catch (ClassNotFoundException cnfE) {
			getLogger().error cnfE.toString()
			throw cnfE
		}
	}

	Sql getSql (String user, String sidHint) {
		if (getSql() == null) {
			createSql (user, sidHint)
		}
		return getSql()
	}

	public Boolean execute (String sqlString, String user, String sidHint) {
		getLogger().info "SQL execution requested for '$sqlString' as user '$user' with sid hint '$sidHint'"
		setCommand(sqlString)
		
		Sql sql;
		Boolean success = false;
		try {
			sql = getSql (user, sidHint)
			getCommands().each { command ->
				sql.execute (command)
				success = true;
			}
		} catch (SQLException sqlE) {
			getLogger().error sqlE
			throw sqlE
		} catch (ClassNotFoundException cnfE) {
			getLogger().error cnfE
			throw cnfE
		}
		return success;
	}

	private Logger getLogger () {
		return getTestCaseManager().getLogger()
	}

	public void setCommand(String command) {
		setCommands([command] as ArrayList)
	}
}
