package com.idexx.soapui.testcase

import com.eviware.soapui.model.testsuite.TestStep
import com.idexx.soapui.properties.PropertyCategory

public class TestCaseWebConnectionManager extends TestCaseConnectionManager {

    public TestCaseWebConnectionManager(TestCaseManager testCaseManager) {
        super(testCaseManager)
    }

    void updateSoapEndpoint(TestStep testStep) {
        String currentEndpoint = testStep.getPropertyValue("Endpoint")
        String endpointPrefixHint =  getTestCaseManager().getPropertyValue("endpointPrefixHint");
        String endpointPrefix = getPropertyManager().getProperty(PropertyCategory.ENVIRONMENT, "endpoint.prefix.${endpointPrefixHint}", testStep)
        String newEndpoint = endpointPrefix;

        String soapInterface =  testStep.getInterface().getName()
        String soapOperation =  testStep.getOperation().getName()
        getLogger().error( soapInterface)
        String endpoinSuffix = getPropertyManager().getProperty(PropertyCategory.ENVIRONMENT, "endpoint.suffix.${soapInterface}.${soapOperation}.${endpointPrefixHint}", testStep)

        getLogger().info "CurrentEndpoint -> ${currentEndpoint}"
        newEndpoint += "/${endpoinSuffix}"
        getLogger().info "New endpoint -> ${newEndpoint}"

        testStep.setPropertyValue("Endpoint", newEndpoint)
    }

    public void updateAllWebConnectionSteps() {
        int currentIndex = getTestCaseManager().getCurrentStepIndex()
        getLogger().info("CurrentStep's Index -> ${currentIndex}")

        getTestCase().getTestSteps().each() { testStepName, testStep ->
            int testStepIndex = getTestCase().getIndexOfTestStep(testStep)

            if (testStepIndex > currentIndex) {

                TestCaseWebConnectionCategory webConnectionCategory = TestCaseWebConnectionCategory.with(testStep);

                getLogger().info("Processing test step [${testStepName}] with index [${testStepIndex}] and connection category ${webConnectionCategory} ")

                switch (webConnectionCategory) {
                    case TestCaseWebConnectionCategory.SOAP_REQUEST:
                        updateSoapEndpoint(testStep);
                        break;

                    default:
                        break;
                }
            }
        }
    }
}
