package com.idexx.soapui.testcase

import com.eviware.soapui.model.testsuite.TestCase
import com.idexx.soapui.properties.PropertyCategory
import com.idexx.soapui.properties.PropertyManager
import org.apache.log4j.Logger

public class TestCaseConnectionManager {

    TestCaseManager testCaseManager

    private TestCaseConnectionManager() {}

    public TestCaseConnectionManager(TestCaseManager testCaseManager) {
        setTestCaseManager(testCaseManager)
        getLogger().info("RequestManager created ->  ${this.getClass().getName()}")
    }

    /*
     * CONVENIENCE METHODS SECTION
     */

    protected Logger getLogger() {
        return getTestCaseManager().getLogger()
    }

    protected PropertyManager getPropertyManager() {
        return getTestCaseManager().getPropertyManager()
    }

    protected Properties getEnvironmentProperties() {
        return getTestCaseManager().getPropertyManager().getPropertiesNamed(PropertyCategory.ENVIRONMENT.name())
    }

    protected TestCase getTestCase() {
        return getTestCaseManager().getTestCase()
    }
}
