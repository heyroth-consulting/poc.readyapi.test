package com.idexx.soapui.testcase

import com.eviware.soapui.model.testsuite.TestStep

enum TestCaseDatabaseConnectionCategory {
    DATA_SINK("WsdlDataSinkTestStep"),
    DATA_SOURCE("WsdlDataSourceTestStep"),
    JDBC_REQUEST("ProJdbcRequestTestStep");

    String name

    private TestCaseDatabaseConnectionCategory(String name) {
        setName(name);
    }

    static TestCaseDatabaseConnectionCategory named(String name) {
        return TestCaseDatabaseConnectionCategory.values().find { it.getName().equals(name) }
    }

    static TestCaseDatabaseConnectionCategory with(TestStep testStep) {
        return TestCaseDatabaseConnectionCategory.named(testStep.getClass().getSimpleName())
    }

    public String toString() {
        StringBuffer sb = new StringBuffer(getClass().getSimpleName() + " -> [");
        sb.append(getName());
        sb.append("]");

        return sb.toString();
    }
}