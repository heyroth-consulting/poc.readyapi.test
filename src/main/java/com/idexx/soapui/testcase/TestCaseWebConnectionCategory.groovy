package com.idexx.soapui.testcase

import com.eviware.soapui.model.testsuite.TestStep

enum TestCaseWebConnectionCategory {

    HTTP_REQUEST("HttpTestRequestStep"),
    REST_REQUEST("RestTestRequestStep"),
    SOAP_REQUEST("WsdlTestRequestStep");

    String name

    private TestCaseWebConnectionCategory(String name) {
        setName(name);
    }

    static TestCaseWebConnectionCategory named(String name) {
        return TestCaseWebConnectionCategory.values().find { it.getName().equals(name) }
    }

    static TestCaseWebConnectionCategory with(TestStep testStep) {
        return TestCaseWebConnectionCategory.named(testStep.getClass().getSimpleName())
    }

    public String toString() {
        StringBuffer sb = new StringBuffer(getClass().getSimpleName() + " -> [");
        sb.append(getName());
        sb.append("]");

        return sb.toString();
    }

}
