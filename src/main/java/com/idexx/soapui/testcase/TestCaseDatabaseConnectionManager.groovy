package com.idexx.soapui.testcase

import com.eviware.soapui.impl.wsdl.teststeps.datasink.AbstractDataSink
import com.eviware.soapui.impl.wsdl.teststeps.datasource.AbstractDataSource
import com.eviware.soapui.model.testsuite.TestCase
import com.eviware.soapui.model.testsuite.TestStep
import com.idexx.soapui.properties.PropertyCategory
import com.idexx.soapui.properties.PropertyManager

import static com.eviware.soapui.support.GroovyUtils.registerJdbcDriver;

public class TestCaseDatabaseConnectionManager extends TestCaseConnectionManager {

    TestCaseDatabaseConnectionManager(TestCaseManager testCaseManager) {
        super(testCaseManager)
    }

    /*
     * CONVENIENCE METHODS SECTION
     */
    private static jdbcDriverName = "oracle.jdbc.driver.OracleDriver"

    public void registerDatabaseDriver () {
        registerJdbcDriver("oracle.jdbc.OracleDriver")
    }

    String getJdbcDriverName(TestStep testStep) {
        if (jdbcDriverName == null) {
            PropertyManager pm = getTestCaseManager().getPropertyManager()
            String jdbcDriverName = pm.getProperty(PropertyCategory.DATABASE, 'jdbc.drivername', testStep)
        }
        return jdbcDriverName
    }

    TestCaseOracleDatabaseInfos dis

    public TestCaseOracleDatabaseInfos getDbInfos() {
        if (dis == null) {
            dis = new TestCaseOracleDatabaseInfos(getTestCaseManager())
        }
        return dis
    }

    public void updateAllDatabaseConnectionSteps(String user, String sidHint) {
        int currentIndex = getTestCaseManager().getCurrentStepIndex()
        getLogger().info("CurrentStep's Index -> ${currentIndex}")

        getTestCase().getTestSteps().each() { testStepName, testStep ->
            // String testStepName = tsa.getKey()
            // TestStep testStep   = tsa.getValue()
            int testStepIndex = getTestCase().getIndexOfTestStep(testStep)

            if (testStepIndex > currentIndex) {
                TestCaseDatabaseConnectionCategory jdbcConnectionCategory = TestCaseDatabaseConnectionCategory.with(testStep)
                def jdbcConnection

                getLogger().info("Checking test step [${testStepName}] with index [${testStepIndex}] and connection category ${jdbcConnectionCategory} ")

                switch (jdbcConnectionCategory) {
                    case TestCaseDatabaseConnectionCategory.DATA_SINK:
                        AbstractDataSink dataSink = testStep.getDataSink()
                        getLogger().info("DataSource -> ${dataSink}")
                        def dataSinkType = dataSink.getType()
                        getLogger().info("DataSourceType -> ${dataSinkType}")
                        if (dataSinkType.equals("JDBC")) {
                            jdbcConnection = dataSink
                        }
                        break

                    case TestCaseDatabaseConnectionCategory.DATA_SOURCE:
                        AbstractDataSource dataSource = testStep.getDataSource()
                        getLogger().info("DataSource -> ${dataSource}")
                        def dataSourceType = dataSource.getType()
                        getLogger().info("DataSourceType -> ${dataSourceType}")
                        if (dataSourceType.equals("JDBC")) {
                            jdbcConnection = dataSource
                        }
                        break

                    case TestCaseDatabaseConnectionCategory.JDBC_REQUEST:
                        jdbcConnection = testStep
                        break

                    default:
                        break
                }

                if (!jdbcConnection.is(null)) {
                    getLogger().info("Setting Attributes in -> ${testStepName}")
                    jdbcConnection.setDriver(getJdbcDriverName())
                    getLogger().info("Driver -> ${jdbcConnection.getDriver()}")
                    jdbcConnection.setConnectionString(getDbInfos().getConnectionString(user, sidHint))
                    getLogger().info("Connectionstring is -> ${jdbcConnection.getConnectionString()}")
                    jdbcConnection.setPassword(getDbInfos().getPassword(user))
                }
            }
        }

        TestCase tc = getTestCaseManager().getTestCase()

        for (TestCaseOracleDatabaseSidHint sh : TestCaseOracleDatabaseSidHint.values()) {
            String shString = sh.getHint()
            String propertyName = "Db"
            propertyName += shString.substring(0, 1).toUpperCase();
            propertyName += shString.substring(1);
            tc.setPropertyValue(propertyName, getDbInfos().getServiceOrSid(shString))
        }
    }
}
