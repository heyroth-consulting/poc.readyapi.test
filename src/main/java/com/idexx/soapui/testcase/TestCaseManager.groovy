package com.idexx.soapui.testcase

import com.eviware.soapui.model.project.Project
import com.eviware.soapui.model.testsuite.TestCase
import com.eviware.soapui.model.testsuite.TestCaseRunContext
import com.eviware.soapui.model.testsuite.TestStep
import com.eviware.soapui.model.testsuite.TestSuite
import com.idexx.soapui.model.TestModelItemManager
import com.idexx.soapui.properties.PropertyManager
import org.apache.log4j.Logger

public class TestCaseManager extends TestModelItemManager {

    TestCaseManager(TestCaseRunContext context, Logger logger) {
        super(context, logger)
    }

    @Override
    Project getProject() {
        return getTestSuite().getProject()
    }

    TestSuite getTestSuite() {
        return getTestCase().getTestSuite();
    }

    private testCase;

    TestCase getTestCase() {
        if (testCase == null) {
            if ((TestCaseRunContext) getContext() != null) {
                testCase = ((TestCaseRunContext) getContext()).getTestCase();
            }
        }
        return testCase
    }

    void setTestCase(TestCase testCase) {
        this.testCase = testCase;
    }

    int getCurrentStepIndex() {
        return ((TestCaseRunContext) getContext()).getCurrentStepIndex();
    }

    TestStep getNextStep() {
        try {
            return getTestCase().getTestStepAt(getCurrentStepIndex() + 1);
        } catch (ArrayIndexOutOfBoundsException) {
            return null
        }
    }

    TestStep getCurrentStep() {
        try {
            return getTestCase().getTestStepAt(getCurrentStepIndex());
        } catch (ArrayIndexOutOfBoundsException) {
            return null
        }
    }

    TestStep getPreviousStep() {
        try {
            return getTestCase().getTestStepAt(getCurrentStepIndex() - 1);
        } catch (ArrayIndexOutOfBoundsException) {
            return null
        }
    }

    TestCaseDatabaseConnectionManager getDatabaseConnectionManager() {
        return new TestCaseDatabaseConnectionManager(this);
    }

    void updateAllDatabaseConnectionSteps(String user, String sidHint) {
        getDatabaseConnectionManager().updateAllDatabaseConnectionSteps(user, sidHint);
    }

    TestCaseWebConnectionManager getWebConnectionManager() {
        return new TestCaseWebConnectionManager(this);
    }

    void updateAllWebConnectionSteps() {
        getWebConnectionManager().updateAllWebConnectionSteps();
    }

    @Override
    public String getBasicPropertyViaModelItem(String propertyName) {
        getLogger().info('Requested basic property' + ' -> ' + propertyName);
        String property = PropertyManager.getProperty(propertyName, (TestStep) getCurrentStep());
        getLogger().info(propertyName + ' (via TestStep) -> ' + property);

        if (property == null) {
            property = PropertyManager.getProperty(propertyName, (TestCase) getTestCase());
            getLogger().info(propertyName + ' (via TestCase) -> ' + property);
        }

        return property
    }

    @Override
    protected Object[] getObjectsToDisplay() {
        return [getTestCase()] as Object[];
    }
}
