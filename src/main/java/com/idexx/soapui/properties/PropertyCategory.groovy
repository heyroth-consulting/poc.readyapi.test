package com.idexx.soapui.properties;

enum PropertyCategory {
	DATABASE ("database", "database"),
	ENVIRONMENT ("environment", "environment"),
	PROJECT ("project", "project");

	String category;
	String filenameBase;

	private PropertyCategory(String category, String filenameBase) {
		setCategory(category);
		setFilenameBase(filenameBase);
	}
}
