package com.idexx.soapui.model

import com.eviware.soapui.model.project.Project
import com.eviware.soapui.model.testsuite.TestRunContext
import com.idexx.soapui.properties.PropertyManager
import com.idexx.test.TestObject
import org.apache.log4j.Logger

abstract class TestModelItemManager extends TestObject implements TestModelItemManagerInterface {

	TestRunContext context;
	Logger logger;

	abstract Project getProject ();
	abstract String getBasicPropertyViaModelItem (String propertyName);

	private TestModelItemManager () {
	}

	TestModelItemManager (TestRunContext context, Logger logger) {
		setContext (context)
		setLogger (logger)
	}

	Properties getProjectPoperties(){
		return getProject().getProperties()
	}

	protected String getBasicProperty(String propertyName){
		getLogger().info('Requested basic property' + ' -> ' + propertyName);
		String property = getBasicPropertyViaModelItem (propertyName);

		if (property == null){
			String projectName = getProject ().getName ();

			String globalProjectPropertyName= '${' + propertyName  + projectName + '}';
			getLogger().info('Requested basic global project property' + ' -> ' + globalProjectPropertyName);
			property = getContext().expand(globalProjectPropertyName);
			getLogger().info(globalProjectPropertyName + ' -> ' + property);
			if (property == null || (property.length() == 0)){
				String globalPropertyName = '${' + propertyName + '}';
				getLogger().info('Requested basic global property' + ' -> ' + globalPropertyName);
				property  = getContext().expand(globalPropertyName );
				getLogger().info(globalPropertyName  + ' -> ' + property );
			}
		}

		return property;
	}

	String getPropertyValue(String propertyName){
		String propertyValue = getBasicProperty(propertyName);
		if (propertyValue != null) {
			propertyValue = getContext().expand(propertyValue);
		}
		return propertyValue;
	}

	private PropertyManager propertyManager;
	PropertyManager getPropertyManager (){
		if (propertyManager == null) {
			String propertiesDirectory = getBasicProperty('PropertiesDirectory');
			if (propertiesDirectory.getAt(propertiesDirectory.length () - 1) != File.separator) {
				propertiesDirectory += File.separator
			}
			String environment = getBasicProperty('Environment');
			propertyManager = new PropertyManager(propertiesDirectory, environment, getLogger());
		}
		return propertyManager ;
	}
}