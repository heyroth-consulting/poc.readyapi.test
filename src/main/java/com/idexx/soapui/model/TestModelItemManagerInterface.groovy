package com.idexx.soapui.model

import com.eviware.soapui.model.testsuite.TestStep
import com.idexx.soapui.properties.PropertyManager

public interface TestModelItemManagerInterface {

	TestStep getCurrentStep ();
	PropertyManager getPropertyManager();
}
