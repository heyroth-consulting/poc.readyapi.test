# Ready API - Common Scripting Library Integration - Proof of Concept

This is a "Proof of Concept" project.

The goal is to provide a common strucure for ReadyAPI projects and a common, reusable scripting library.

Due to several shortcomings in the ReadyAPI environment management, we decided to try out
* Environment management via scripting library
* Provisioning of a common Grooyv scripting library
* Integration of the scripting library with multiple ReadyAPI projects

# Directory Structure
* **Project directory**  
  * *pom.xml*
    * **src/main/java**
      * *The scritping library Groovy code*
    * **src/test/readyapi**
      * *The ReadyAPI test projects*
    * **src/test/resources**
      * *The test resources*

# Installation
* Clone the repository to your local drive
* Open and configure ReadyAPI
   * Open the ReadyAPI settings
   * Under **ReadyAPI** ensure that
     * Sript Library=${projectDir}/src/main/java
   * Under **Global Properties** ensure that 
     * PropertiesDirectory=${projectDir}/../resources/properties/
     * Environment ∈ {dev1, dev2, qa1, qa2, stage, prod}
     
# Features
The current version of the scripting library provied for to main features. These are bothe based on the same prociple.

Certain TestSteps of a TestCase can be checked and have specific porperties updated, based on the acrive environment and the content of the corresponding propertiy files.
## Database Connections
These connections currently refer to
* DataSource
* DataSink
* JDBC Request
When one of these test steps is used, the scripting library will update any contained database connections, with the values specied via property files.

## WebConnections
These connections currently refer to
* SoapReuqest
When one of these test steps is used, the scripting library will update their enpointd, with the values specied via property files.

## Property Evaluation
By default on can access the properties of a specific **TestModel** (Project, Suite, Case, Step).

The scpriting library provides a lookup mechanism, that searches for a requested property upwards in the hirarchy.

E. g. I want to query the current environment, i would ustilie the following code:

    import com.idexx.soapui.testcase.*
        
    def tcm = new TestCaseManager (context, log)
    def environment = tcm.getPropertyValue("Environment")

This sample will 
* first try to retrieve the property value via lookup in the current TestStep
* then via lookup in the precediding TestCase
* then via lookup in the precediding TesSuite
* then via lookup in the enclosing TestProject
* and finally via lookup in the global properties

This for example enables you to specify a commonm or default environment for all projects and thne overide that on the projet level.

So when you usually execute your test in **dev** environment you would make that your default via the global properties.

To execute a specific projet in a **qa** environment you would defin that in that specific project.

#Examples
## Connections Example
In order to update all \*Connection TestSteps based on environment and property file, a **Groovy Script** TestStep mus be used as the first TestStep od a TestCase.

    import com.idexx.soapui.testcase.*
    
    def tcm = new TestCaseManager (context, log)
    tcm.updateAllDatabaseConnectionSteps ('LYNXX', 'default')
    tcm.updateAllWebConnectionSteps() 

This script will update evaluate all subsequent TestSteps and update the, if appropriate.

# PropertyManager

The **com.idexx.soapui.properties.PropertyCategory.PropertyManager** manages system wide property files, which fall into 3 categories
* **com.idexx.soapui.properties.PropertyCategory.DATABASE**
  * These are database specicif properties **database[.<environment>].properties**
* **com.idexx.soapui.properties.PropertyCategory.ENVIRONMENT**
  * these are environment specific properties **environment[.<environment>].properties**
* **com.idexx.soapui.properties.PropertyCategory.PROJECT**
  * these are project wide properties  **project[.<environment>].properties**

The **PropertyManager+* will evalute standard property files in the **PropertiesDirectory** and for the environment that have been specified when its constructor was called.

To access a property within a property file use	**PropertyManager>>getProperty (category, key, teststep)**

This method will load and cache all properties from the category specific property files when called for the first time.
* first by checking and if present loading and caching all values from **\<propertyCategory>.properties**
* they by checking and if present loading and caching all values from **\<propertyCategory>.\<environment>.properties**
  * this step will override all values from the first step

# Maven/Jenkins Integration
For Jenkins integration a standard prom has been provided.

To execute any test the **verify** or **test** targets can be used.

To further control executeion the following properties are defined and can be overridden upon execution.

* **test.environment**
  * default: **undefined**
* **test.project.dir**
  * default: **src/test/readyapi**
* **test.project.file**
  * default: **ReadyAPI-Framework-Demo-readyapi-project.xml**
* **test.testCase**
  * default: **undefined**
  * valid profiles: **singelTestCase**
* **test.testSuite**
  * default: **undefined**
  * valid profiles: **singelTestCase**, **singleTestSuite**

A sample Jenkins integration can be found for [SAP-International-Petcheck-readyapi-project](http://wmebld2.idexxi.com:9000/job/LYNXX/job/Builds/job/TESTS/job/Service%20Layer%20Integration-SAP%20International%20Pet%20Check/)
